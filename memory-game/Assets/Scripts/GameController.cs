﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public List<Button> cartas = new List<Button>();
    public int noCartas = 12;
    /* https://docs.unity3d.com/ScriptReference/MonoBehaviour.Awake.html */
    [SerializeField]
    private Transform campo;

    [SerializeField]
    private GameObject btn;
    
    void Awake() => initCartas();

    // Start is called before the first frame update
    void Start()
    {
        getCartas();
        addListeners();
    }
    public void clicouNaCarta()
    {
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
        Debug.Log("Clicou em " + name);
    }
    void addListeners()
    {
        foreach (Button carta in cartas)
        {
            carta.onClick.AddListener(() => clicouNaCarta());
        }
    }
    void getCartas()
    {
        /* Seta a lista de cartas */
        GameObject[] cc = GameObject.FindGameObjectsWithTag("Carta");

        foreach (GameObject c in cc)
        {
            cartas.Add(c.GetComponent<Button>());
        }
    }
    void initCartas()
    {
        /* Método que cria as cartas no GRID */
        for (int i = 0; i < noCartas; i++)
        {
            GameObject button = Instantiate(btn);
            button.name = "" + i;
            button.transform.SetParent(campo, false);
        }
    }
}
